use websockets::structures::web_socket_stream::{WebSocketStream, WebSocketWriterExt};
use std::io::{BufReader, BufRead, Write};
use std::io;
use websockets::enums::message::Message::Ping;

const CONNECT_SOCKET_ADDR: &str = "127.0.0.1:7977";

fn main() {
    let mut stream = WebSocketStream::connect(CONNECT_SOCKET_ADDR,
    "/path", CONNECT_SOCKET_ADDR, "www.trywebsocket.com").expect("Failed to connect");
    let mut reader = BufReader::new(stream.reader);
    println!("Successfully connected to server {}", CONNECT_SOCKET_ADDR);
    match run(&mut reader, &mut stream.writer) {
        Ok(_) => (),
        Err(err) => panic!(err),
    }
    println!("Terminated.");
}

fn run<R: BufRead, W: Write>(reader: &mut R, writer: &mut WebSocketWriterExt<W>) -> io::Result<()> {
    let mut string = String::new();
    let mut buf = [0u8; 150];
    loop {
        writer.send_web_socket_message(Ping("ping".as_bytes()))?;
        io::stdin().read_line(&mut string)?;
        writer.write(format!("{}", string.trim_end()).as_bytes())?;
        string.clear();
        let length = reader.read(&mut buf)?;
        let response = std::str::from_utf8(&buf[..length]).unwrap();
        println!("received: {}", response);
    }
}