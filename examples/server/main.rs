use std::net::TcpListener;
use std::io::{BufReader, BufRead, Write};
use std::io;
use websockets::structures::web_socket_stream::WebSocketStream;

const LISTENING_SOCKET_ADDR: &str = "127.0.0.1:7977";


fn main() {
    let listener = TcpListener::bind(LISTENING_SOCKET_ADDR).unwrap();
    println!("listen {}", LISTENING_SOCKET_ADDR);

    for stream in WebSocketStream::incoming(&listener) {
        match stream {
            Ok(mut stream) => {
                println!("Incoming connection {}", stream.peer_addr().unwrap().ip());
                let mut reader = BufReader::new(stream.reader);
                match run(&mut reader, &mut stream.writer) {
                    Ok(_) => (),
                    Err(err) => panic!(err),
                }
            }
            Err(error) => println!("connection establishment error: {:?}", error),
        }
        println!("Incoming connection terminated");
    }
}

fn run<R: BufRead, W: Write>(reader: &mut R, writer: &mut W) -> io::Result<()> {
    let mut buf = [0u8;150];
    loop {
        let length = reader.read(&mut buf)?;
        match std::str::from_utf8(&buf[..length]) {
            Ok(string) => {
                println!("received: {}", string);
                writer.write(format!("echo: {}", string).as_bytes())?;
            },
            Err(err) => {
                print!("{}", std::str::from_utf8(&buf[..err.valid_up_to()]).unwrap());
                println!("{:02x?}", &buf[..length]);
            }
        }
    }
}