use std::net::{TcpListener};
use std::io::{BufReader, BufRead, Write};
use std::io;
use websockets::structures::web_socket_stream::WebSocketStream;
use x509::utils::file_util::{read_private_key, read_certificate_file};

const LISTENING_SOCKET_ADDR: &str = "127.0.0.1:7977";

fn main() {
    let listener = TcpListener::bind(LISTENING_SOCKET_ADDR).unwrap();
    println!("listen {}", LISTENING_SOCKET_ADDR);

    let mut certificates_chain: Vec<&[u8]> = Vec::with_capacity(2);
    let server_certificate= read_certificate_file("res/trytls2.cer").expect("< can't read certificate");
    let ca_certificate = read_certificate_file("res/trytlsca.cer").expect("< can't read ca certificate");
    certificates_chain.push(&server_certificate);
    certificates_chain.push(&ca_certificate);
    let private_key = read_private_key("res/ecdsa-private-key.pem", "ecdsa");

    for stream in WebSocketStream::incoming_secure(&listener, &certificates_chain, &private_key).into_iter() {
        match stream {
            Ok(mut stream) => {
                println!("Incoming connection {}", stream.peer_addr().unwrap().ip());
                let mut reader = BufReader::new(stream.reader);
                match run(&mut reader, &mut stream.writer) {
                    Ok(_) => (),
                    Err(err) => panic!(err),
                }
            }
            Err(error) => println!("connection establishment error: {:?}", error),
        }
        println!("Incoming connection terminated");
    }
}

fn run<R: BufRead, W: Write>(reader: &mut R, writer: &mut W) -> io::Result<()> {
    let mut buf = [0u8; 150];
    loop {
        let length = reader.read(&mut buf)?;
        match std::str::from_utf8(&buf[..length]) {
            Ok(string) => {
                println!("received: {}", string);
                writer.write(format!("echo: {}", string).as_bytes())?;
            }
            Err(err) => {
                print!("{}", std::str::from_utf8(&buf[..err.valid_up_to()]).unwrap());
                println!("{:02x?}", &buf[..length]);
            }
        }
    }
}