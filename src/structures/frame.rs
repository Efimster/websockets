use crate::enums::op_code::OpCode;
use std::mem;
use std::io::{Read, Bytes};
use core::cmp;
use crate::enums::web_socket_error::WebSocketError;

pub struct Frame {
    pub fin: bool,
    pub op_code: OpCode,
    pub full_len: usize,
    pub masking_key: Option<[u8; 4]>,
    pub payload: Vec<u8>,
}

pub struct FrameMeta {
    pub fin: bool,
    pub op_code: OpCode,
    pub full_len: usize,
    pub masking_key: Option<[u8; 4]>,
}

const MAX_BUFFER_SIZE: usize = 1200;

impl Frame {
    pub fn to_bytes(&self) -> Vec<u8> {
        let (masking_bit, masking_key_len) = match self.masking_key {
            None => (0x00u8, 0u8),
            Some(_) => (0x80, 4u8)
        };

        let (extended_length, mask_and_length) = if self.full_len < 126 {
            (0u8, self.full_len as u8 | masking_bit)
        } else if self.full_len < u16::max_value() as usize {
            (2u8, (126u8 | masking_bit) as u8)
        } else {
            (8u8, 127u8 | masking_bit)
        };

        let mut frame: Vec<u8> = Vec::with_capacity(2usize + extended_length as usize
            + self.full_len as usize + masking_key_len as usize);
        let fin: u8 = if self.fin { 0x80 } else { 0x00 };

        frame.push(self.op_code.to_u8() | fin);
        frame.push(mask_and_length);

        if extended_length == 2 {
            let full_len: u16 = self.full_len as u16;
            let len: [u8; 2] = unsafe { mem::transmute(full_len.to_be()) };
            frame.extend_from_slice(&len);
        } else if extended_length == 8 {
            let len: [u8; 8] = unsafe { mem::transmute(self.full_len.to_be()) };
            frame.extend_from_slice(&len);
        }

        match self.masking_key {
            None => (),
            Some(key) => frame.extend_from_slice(&key)
        }

        frame.extend_from_slice(&self.payload);
        frame
    }

    pub fn complete(&self) -> bool {
        self.full_len == self.payload.len()
    }


    pub fn from_stream<T: Read>(stream: &mut T) -> Result<Frame, WebSocketError> {
        let meta = Self::get_frame_meta(stream)?;

        let mut buffer = [0u8; MAX_BUFFER_SIZE];
        let mut payload = Vec::with_capacity(meta.full_len);
        let left_size = cmp::min(meta.full_len, MAX_BUFFER_SIZE);
        if left_size > 0 {
            let read_size = stream.read(&mut buffer[..left_size])?;
            Self::append_bytes(&mut payload, &mut buffer[..read_size], meta.masking_key)?;
        }

        Ok(Frame {
            fin: meta.fin,
            op_code: meta.op_code,
            full_len: meta.full_len,
            masking_key: meta.masking_key,
            payload,
        })
    }

    pub fn get_frame_meta<T: Read>(stream: &mut T) -> Result<FrameMeta, WebSocketError> {
        let iterator = &mut stream.bytes();
        let first_byte = match iterator.next() {
            Some(value) => value,
            None => {
                return Err(WebSocketError::EmptyFrame);
            }
        }?;
        let fin = first_byte & 0x80 > 0;
        let op_code = OpCode::from_u8(first_byte & 0x0f)?;

        let second_byte = match iterator.next() {
            Some(value) => value,
            None => {
                return Err(WebSocketError::MalformedFrame);
            }
        }?;

        let full_len = Self::read_meta_length(second_byte, iterator)?;

        let masking_key = if second_byte & 0x80 > 0 {
            let mut masking_key: [u8; 4] = [0; 4];

            for (index, item) in iterator.enumerate().take(4) {
                masking_key[index] = item?;
            }
            Some(masking_key)
        } else {
            None
        };

        Ok(FrameMeta {
            fin,
            op_code,
            full_len,
            masking_key,
        })
    }

    fn read_meta_length<T: Read>(second_byte: u8, iterator: &mut Bytes<T>) -> Result<usize, WebSocketError> {
        if second_byte & 0x7f < 126 {
            return Ok((second_byte & 0x7f) as usize);
        }

        let field_len: usize = match second_byte & 0x7f {
            126 => 2,
            127 => 8,
            _ => return Err(WebSocketError::MalformedFrameLength),
        };

        iterator.enumerate().take(field_len)
            .fold(Ok(0usize), |acc: Result<usize, WebSocketError>, (index, item)| {
                let result = acc.unwrap() | ((item? as usize) << (field_len - 1 - index << 3));
                Ok(result)
            })
    }

    pub fn append_bytes(dest: &mut Vec<u8>, src: &mut [u8], masking_key: Option<[u8; 4]>) -> Result<(), WebSocketError> {
        if let Some(masking_key) = masking_key {
            let mut masking_key = masking_key;
            masking_key.rotate_left(dest.len() % 4);

            let (prefix, aligned, suffix) = unsafe { src.align_to_mut::<u32>() };
            for index in 0..prefix.len() {
                prefix[index] ^= masking_key[index % 4];
            }
            masking_key.rotate_left(prefix.len() % 4);
            {
                let masking_key: u32 = unsafe { mem::transmute(masking_key) };
                for index in 0..aligned.len() {
                    aligned[index] ^= masking_key;
                }
            }
            for index in 0..suffix.len() {
                suffix[index] ^= masking_key[index % 4];
            }
        }

        dest.extend_from_slice(src);
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::BufReader;

    #[test]
    fn test_from_bytes() {
        let bytes: &[u8] = &[0x81u8, 0x8bu8, 0xe9, 0x6a, 0xd5, 0xac, 0xd8, 0x58, 0xe6, 0x98,
            0xd8, 0x58, 0xe6, 0x98, 0xd8, 0x58, 0xe6];

        let mut reader = BufReader::with_capacity(30, bytes);
        let frame = Frame::from_stream(&mut reader).unwrap();

        assert_eq!(frame.fin, true);
        assert_eq!(frame.op_code, OpCode::Text);
        assert_eq!(frame.full_len, 11);
        assert_eq!(frame.masking_key, Some([0xe9, 0x6a, 0xd5, 0xac]));
        assert_eq!(frame.payload, [0x31, 0x32, 0x33, 0x34, 0x31, 0x32, 0x33, 0x34, 0x31, 0x32, 0x33]);
        assert_eq!(frame.full_len, frame.payload.len());

        let bytes: &[u8] = &[0x82u8, 0xfe, 0x00, 0x8a, 0xe9, 0x6a, 0xd5, 0xac, 0xd8, 0x58, 0xe6, 0x98,
            0xd8, 0x58, 0xe6, 0x98, 0xd8, 0x58, 0xe6];
        let mut reader = BufReader::with_capacity(2, bytes);
        let frame = Frame::from_stream(&mut reader).unwrap();
        assert_eq!(frame.op_code, OpCode::Binary);
        assert_eq!(frame.full_len, 138);
    }

    #[test]
    fn test_append_bytes() {
        let bytes: &[u8] = &[0x81u8, 0x8eu8, 0xe9, 0x6a, 0xd5, 0xac, 0xd8, 0x58, 0xe6, 0x98,
            0xd8, 0x58, 0xe6, 0x98, 0xd8, 0x58, 0xe6];
        let mut reader = BufReader::with_capacity(2, bytes);
        let mut frame = Frame::from_stream(&mut reader).unwrap();

        assert!(frame.full_len > frame.payload.len());

        Frame::append_bytes(&mut frame.payload, &mut [0x98, 0xd8, 0x58], frame.masking_key).unwrap();
        assert_eq!(frame.full_len, frame.payload.len());
        let message = String::from_utf8(frame.payload).unwrap();
        assert_eq!(message, "12341234123412");
    }

    #[test]
    fn test_to_bytes() {
        let payload = "1234".as_bytes();

        let mut frame = Frame {
            fin: true,
            op_code: OpCode::Text,
            masking_key: None,
            full_len: payload.len(),
            payload: payload.to_vec()
        };

        let bytes = frame.to_bytes();
        assert_eq!(bytes, [0x81u8, 0x04, 0x31, 0x32, 0x33, 0x34]);

        frame.full_len = 138;
        let bytes = frame.to_bytes();
        assert_eq!(&bytes[..4], [0x81u8, 0x7e, 0x00, 0x8a]);
    }

    #[test]
    fn test_few_frames_in_a_row() {
        let mut input: Vec<u8> = Vec::new();
        input.extend_from_slice(&[0x81u8, 0x01u8, 0x01]);
        input.extend_from_slice(&[0x81u8, 0x01u8, 0x02]);
        input.extend_from_slice(&[0x81u8, 0x01u8, 0x03]);


        let mut reader = BufReader::with_capacity(30, input.as_slice());
        let frame = Frame::from_stream(&mut reader).unwrap();
        assert_eq!(frame.payload[0], 0x01);
        let frame = Frame::from_stream(&mut reader).unwrap();
        assert_eq!(frame.payload[0], 0x02);
        let frame = Frame::from_stream(&mut reader).unwrap();
        assert_eq!(frame.payload[0], 0x03);
    }

    #[test]
    fn test_empty_stream() {
        let input: Vec<u8> = Vec::new();
        let mut reader = BufReader::with_capacity(30, input.as_slice());
        let result = Frame::from_stream(&mut reader);
        let mut compare = false;

        if let Err(_) = result {
            compare = true;
        }

        assert!(compare);
    }
}