use std::net::{TcpStream, ToSocketAddrs, TcpListener, SocketAddr};
use crate::enums::web_socket_error::WebSocketError;
use std::io::{Read, Write};
use std::io;
use crate::structures::web_socket_reader::WebSocketReader;
use crate::structures::web_socket_writer::WebSocketWriter;
use crate::structures::http::http_handshake;
use std::time::Duration;
use crate::enums::message::Message;
use std::sync::{Arc, Mutex};
use std::ops::DerefMut;


pub struct WebSocketStream<R:Read, W:Write> {
    pub reader: WebSocketReader<R>,
    pub writer: WebSocketWriterExt<W>,
    pub peer_addr: SocketAddr,
}

impl WebSocketStream<TcpStream, TcpStream> {
    pub fn connect<A: ToSocketAddrs>(addr: A, path: &str, host: &str, origin: &str) -> Result<WebSocketStream<TcpStream, TcpStream>, WebSocketError> {
        let mut stream = TcpStream::connect(addr)?;
        let mut reader = stream.try_clone()?;
        let peer_addr = stream.peer_addr()?;
        http_handshake::outgoing_http_handshake(&mut reader, &mut stream, path, host, origin)?;
        let writer = Arc::new(Mutex::new(WebSocketWriter::new(stream)));
        let mut reader = WebSocketReader::new(reader);

        let writer_clone = Arc::clone(&writer);
        let handler = move || {
            Self::on_ping_frame(writer_clone.lock().unwrap().deref_mut())
        };
        reader.register(handler);

        let stream = WebSocketStream {
            reader,
            writer:WebSocketWriterExt(writer),
            peer_addr,
        };

        Ok(stream)
    }

    pub fn incoming<'a>(listener: &'a TcpListener) -> WrapperIncoming<'a> {

        WrapperIncoming {
            listener,
            ping_callback: Self::on_ping_frame,
        }
    }

    pub fn set_read_timeout(&self, dur: Option<Duration>) -> io::Result<()> {
        self.reader.set_read_timeout(dur)
    }
}

impl<R:Read, W: Write> WebSocketStream<R, W> {
    pub fn on_ping_frame(writer:&mut WebSocketWriter<W>) -> Result<(), WebSocketError> {
        writer.send_web_socket_message(Message::Pong(&[]))?;
        Ok(())
    }

    pub fn peer_addr(&self) -> io::Result<SocketAddr> {
        Ok(self.peer_addr)
    }
}

impl<R: Read, W: Write> Read for WebSocketStream<R, W> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.reader.read(buf)
    }
}

impl<R: Read, W: Write> Write for WebSocketStream<R, W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        Ok(self.writer.write(buf)?)
    }
    fn flush(&mut self) -> io::Result<()> {
        Ok(self.writer.flush()?)
    }
}

pub struct WrapperIncoming<'a> {
    listener: &'a TcpListener,
    ping_callback: fn(&mut WebSocketWriter<TcpStream>) -> Result<(), WebSocketError>,
}

impl<'a, > Iterator for WrapperIncoming<'a>
{
    type Item = Result<WebSocketStream<TcpStream, TcpStream>, WebSocketError>;

    fn next(&mut self) -> Option<Self::Item> {
        let result = match self.listener.accept() {
            Ok((mut stream, socket)) => {
                let mut reader = stream.try_clone().unwrap();
                match http_handshake::incoming_http_handshake(&mut reader, &mut stream) {
                    Ok(_) => {
                        let writer = Arc::new(Mutex::new(WebSocketWriter::new(stream)));
                        let mut reader = WebSocketReader::new(reader);

                        let writer_clone = Arc::clone(&writer);
                        let f = self.ping_callback;
                        let handler = move || {
                            f(writer_clone.lock().unwrap().deref_mut())
                        };
                        reader.register(handler);

                        let stream = WebSocketStream {
                            reader,
                            writer:WebSocketWriterExt::new(writer),
                            peer_addr:socket,
                        };

                        Ok(stream)
                    },
                    Err(err) => Err(err),
                }
            },
            Err(e) => Err(e.into()),
        };

        Some(result)
    }
}

pub struct WebSocketWriterExt<W:Write>(Arc<Mutex<WebSocketWriter<W>>>);

impl<W:Write> Write for WebSocketWriterExt<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        Ok(self.write(buf)?)
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(self.flush()?)
    }
}

impl<W:Write> WebSocketWriterExt<W>{
    pub fn new(inner: Arc<Mutex<WebSocketWriter<W>>>) -> WebSocketWriterExt<W>{
        WebSocketWriterExt(inner)
    }

    pub fn send_web_socket_message(&mut self, message: Message) -> Result<usize, WebSocketError> {
        self.0.lock()?.send_web_socket_message(message)
    }

    fn write(&mut self, buf: &[u8]) -> Result<usize, WebSocketError> {
        Ok(self.0.lock()?.write(buf)?)
    }

    fn flush(&mut self) -> Result<(), WebSocketError> {
        Ok(self.0.lock()?.flush()?)
    }
}

