use std::io::Write;
use crate::enums::op_code::OpCode;
use crate::structures::frame::Frame;
use crate::enums::message::Message;
use std::io;
use std::net::{TcpStream, Shutdown};
use crate::enums::web_socket_error::WebSocketError;
use tls::structs::tls_writer::TLSWriter;

const MAX_BUFFER_SIZE: usize = 1200;

pub struct WebSocketWriter<W: Write> {
    writer: W,
}

impl<W:Write> Write for WebSocketWriter<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize>{
        Ok(self.send_web_socket_message(Message::Text(buf))?)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.writer.flush()
    }
}

impl<W:Write> WebSocketWriter<W> {
    pub fn new(writer:W) -> WebSocketWriter<W> {
        WebSocketWriter {
            writer,
        }
    }

    pub fn send_web_socket_message(&mut self, message: Message) -> Result<usize, WebSocketError> {
        let (body, op_code) = match message {
            Message::Ping(data) => (data, OpCode::Ping),
            Message::Pong(data) => (data, OpCode::Pong),
            Message::Close(data) => (data, OpCode::Close),
            Message::Text(data) => (data, OpCode::Text),
            Message::Binary(data) => (data, OpCode::Binary),
        };

        let max_buffer_size = MAX_BUFFER_SIZE;
        let mut partitions = body.len() / max_buffer_size
            + (body.len() % max_buffer_size > 0) as usize;

        if partitions == 0 || op_code.is_heartbeat(){
            partitions = 1;
        }

//        println!("sending {:?} message in {} frames", op_code, partitions);
        let mut length = 0usize;
        for partition in 0..partitions {
            let fin: bool = partition == partitions - 1;
            let start_byte = partition * max_buffer_size;
            let bytes: &[u8] = if !fin {
                &body[start_byte..start_byte + max_buffer_size]
            } else {
                &body[start_byte..body.len()]
            };

            let op_code = if partition == 0 { op_code} else { OpCode::Continuation};

            let frame = Frame {
                fin,
                op_code,
                masking_key: None,
                full_len: bytes.len(),
                payload: bytes.to_vec()
            };

//            println!("frame ({}) fin={} payload={}", partition, fin, String::from_utf8(bytes.into()).unwrap());
            length += self.writer.write(&frame.to_bytes())?;
        }

        self.writer.flush()?;
        Ok(length)
    }


}

impl WebSocketWriter<TcpStream> {
    pub fn shutdown(&self) -> io::Result <()>{
        self.writer.shutdown(Shutdown::Write)
    }
}

impl WebSocketWriter<TLSWriter<TcpStream>> {
    pub fn shutdown(&self) -> io::Result<()> {
        self.writer.shutdown()
    }
}