use std::io::{Read, Write};
use crate::enums::web_socket_error::WebSocketError;
use sha::sha1;
use http::structures::http_header::HttpHeader;
use http::structures::http_header;
use http::structures::http_message::HttpMessage;

const RESPONSE_STATUS_LINE: &str = "HTTP/1.1 101 Switching Protocols";
const WS_GUID: &str = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

pub fn incoming_http_handshake<R: Read, W: Write>(stream: &mut R, writer: &mut W) -> Result<(), WebSocketError> {
    let request = HttpMessage::read(stream)?;
    if !request.contains_header(http_header::HOST) || !request.contains_header(http_header::UPGRADE)
        || !request.contains_header(http_header::WEBSOCKET_VERSION) || !request.contains_header(http_header::WEBSOCKET_KEY) {
        return Err(WebSocketError::MissingHandshakeParameter);
    }
    let accept_key = make_accept_key(request.get_header(http_header::WEBSOCKET_KEY).unwrap().value.as_str());
    let request = HttpMessage {
        status_line: RESPONSE_STATUS_LINE.to_string(),
        headers: Box::new([
            HttpHeader::from_key_value(http_header::CONNECTION, "Upgrade"),
            HttpHeader::from_key_value(http_header::WEBSOCKET_ACCEPT_KEY, accept_key.as_str()),
            HttpHeader::from_key_value(http_header::UPGRADE, "websocket")
        ]),
        payload: Box::new([]),
    };
    writer.write(&request.as_bytes())?;
    Ok(())
}

fn make_accept_key(key: &str) -> String {
    let result = sha1::encode(format!("{}{}", key, WS_GUID).as_bytes());
    let result = base64::encode_bytes(&result);
    std::str::from_utf8(&result).unwrap().to_string()
}

pub fn outgoing_http_handshake<R: Read, W: Write>(reader: &mut R, writer: &mut W,
                                              path: &str, host: &str, origin: &str) -> Result<(), WebSocketError>
{
    let headers = [
        HttpHeader::from_key_value(http_header::HOST, &host),
        HttpHeader::from_key_value(http_header::ORIGIN, &origin),
        HttpHeader::from_key_value(http_header::CONNECTION, "Upgrade"),
        HttpHeader::from_key_value(http_header::UPGRADE, "websocket"),
        HttpHeader::from_key_value(http_header::WEBSOCKET_KEY, generate_key().as_str()),
        HttpHeader::from_key_value(http_header::WEBSOCKET_VERSION, "13"),
        HttpHeader::from_key_value(http_header::WEBSOCKET_PROTOCOL, ""),
        HttpHeader::from_key_value(http_header::WEBSOCKET_EXTENSIONS, ""),
    ];
    let request = HttpMessage {
        status_line: format!("GET {} HTTP/1.1", path),
        headers: Box::new(headers),
        payload: Box::new([]),
    };
    writer.write(&request.as_bytes())?;
    let response = HttpMessage::read(reader)?;
    if response.status_line.trim_end() == RESPONSE_STATUS_LINE {
        Ok(())
    } else {
        Err(WebSocketError::HandshakeRejected)
    }
}

fn generate_key() -> String {
    let result = sha1::encode(&random::generate_bytes());
    let result = base64::encode_bytes(&result);
    std::str::from_utf8(&result).unwrap().to_string()
}