use std::net::{TcpStream, ToSocketAddrs, TcpListener};
use crate::enums::web_socket_error::WebSocketError;
use crate::structures::web_socket_reader::WebSocketReader;
use crate::structures::web_socket_writer::WebSocketWriter;
use crate::structures::http::http_handshake;
use tls::structs::tls_reader::TLSReader;
use tls::structs::tls_writer::TLSWriter;
use tls::structs::tls_stream::{TLSStream, Wrapper};
use x509::private_key::PrivateKey;
use crate::structures::web_socket_stream::{WebSocketStream, WebSocketWriterExt};
use std::sync::{Arc, Mutex};
use std::ops::DerefMut;
use std::time::Duration;
use std::io;


impl WebSocketStream<TLSReader<TcpStream>, TLSWriter<TcpStream>> {
    pub fn connect_secure<A: ToSocketAddrs>(addr: A, path: &str, host: &str, origin: &str) -> Result<WebSocketStream<TLSReader<TcpStream>, TLSWriter<TcpStream>,>, WebSocketError> {
        match TLSStream::connect(addr) {
            Ok(mut stream) => {
                let peer_addr = stream.peer_addr()?;
                http_handshake::outgoing_http_handshake(&mut stream.reader, &mut stream.writer, path, host, origin)?;
                let writer = Arc::new(Mutex::new(WebSocketWriter::new(stream.writer)));
                let mut reader = WebSocketReader::new(stream.reader);

                let writer_clone = Arc::clone(&writer);
                let handler = move || {
                    Self::on_ping_frame(writer_clone.lock().unwrap().deref_mut())
                };
                reader.register(handler);

                Ok(WebSocketStream {
                    reader,
                    writer:WebSocketWriterExt::new(writer),
                    peer_addr,
                })
            }
            Err(e) => Err(e.into()),
        }
    }

    pub fn incoming_secure<'a>(listener: &'a TcpListener, certificates: &'a [&'a [u8]], private_key: &'a PrivateKey) -> SecureWrapperIncoming<'a> {
        SecureWrapperIncoming {
            iterator: TLSStream::incoming(listener, certificates, private_key),
            ping_callback: Self::on_ping_frame,
        }
    }

    pub fn set_read_timeout(&self, dur: Option<Duration>) -> io::Result<()> {
        self.reader.set_read_timeout(dur)
    }
}

pub struct SecureWrapperIncoming<'a> {
    iterator: Wrapper<'a>,
    ping_callback: fn(&mut WebSocketWriter<TLSWriter<TcpStream>>) -> Result<(), WebSocketError>,
}

impl Iterator for SecureWrapperIncoming<'_>
{
    type Item = Result<WebSocketStream<TLSReader<TcpStream>, TLSWriter<TcpStream>>, WebSocketError>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.iterator.next() {
            Some(item) => {
                let result = match item {
                    Ok(mut tls_stream) => {
                        let peer_addr = tls_stream.peer_addr().unwrap();
                        match http_handshake::incoming_http_handshake(&mut tls_stream.reader, &mut tls_stream.writer) {
                            Ok(_) => {
                                let writer = Arc::new(Mutex::new(WebSocketWriter::new(tls_stream.writer)));
                                let mut reader = WebSocketReader::new(tls_stream.reader);

                                let writer_clone = Arc::clone(&writer);
                                let f = self.ping_callback;
                                let handler = move || {
                                    f(writer_clone.lock().unwrap().deref_mut())
                                };
                                reader.register(handler);
                                let stream = WebSocketStream {
                                    reader,
                                    writer:WebSocketWriterExt::new(writer),
                                    peer_addr,
                                };

                                Ok(stream)
                            },
                            Err(err) => Err(err),
                        }
                    },
                    Err(err) => Err(err.into()),
                };
                Some(result)
            },
            None => None,
        }
    }
}


