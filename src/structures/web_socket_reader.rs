use std::io::{Read, Write};
use std::io;
use crate::enums::web_socket_error::WebSocketError;
use crate::structures::frame::Frame;
use core::{cmp};
use crate::enums::op_code::OpCode;
use std::net::{TcpStream, Shutdown};
use std::time::Duration;
use tls::structs::tls_reader::TLSReader;

const MAX_BUFFER_SIZE: usize = 1200;

pub struct WebSocketReader<R:Read> {
    reader: R,
    buffer: Box<[u8]>,
    ping_callback: Option<Box<dyn FnMut() -> Result<(), WebSocketError> + 'static + Send + Sync>>,
}

impl<R: Read> Read for WebSocketReader<R> {
    fn read(&mut self, mut buf: &mut [u8]) -> io::Result<usize> {
        let length = if self.buffer.len() > 0 {
            let length = buf.write(&self.buffer)?;
            self.buffer = Box::from(&self.buffer[length..]);
            length
        }
        else {
            let message = self.read_web_socket_message()?;
            let length = buf.write(&message[..])?;
            self.buffer = Box::from(&message[length..]);
            length
        };
        Ok(length)
    }
}

impl<R:Read> WebSocketReader<R> {
    pub fn new(reader:R) -> WebSocketReader<R>{
        WebSocketReader {
            reader,
            ping_callback: None,
            buffer: Box::new([]),
        }
    }

    fn read_web_socket_frame_from_stream(&mut self) -> Result<Frame, WebSocketError> {
        let mut frame = Frame::from_stream(&mut self.reader)?;
        if frame.complete() {
            return Ok(frame);
        }

        let mut buffer = [0u8; MAX_BUFFER_SIZE];
        loop {
            let left_size = cmp::min(frame.full_len - frame.payload.len(), MAX_BUFFER_SIZE);
            let read_size = self.reader.read(&mut buffer[..left_size])?;
            Frame::append_bytes(&mut frame.payload, &mut buffer[..read_size], frame.masking_key)?;
            if frame.complete() {
                break;
            }
        }
        Ok(frame)
    }

    pub fn read_web_socket_message(&mut self) -> Result<Box<[u8]>, WebSocketError> {
        let mut message: Vec<u8> = Vec::new();
        loop {
            let frame = self.read_web_socket_frame_from_stream()?;
            match frame.op_code {
                OpCode::Text | OpCode::Binary | OpCode::Continuation => {
                    message.extend_from_slice(&frame.payload);
                    if frame.fin {
                        return Ok(message.into_boxed_slice());
                    }
                }
                OpCode::Ping => match self.ping_callback {
                    Some(ref mut callback) => callback()?,
                    None => (),
                },
                OpCode::Pong => (),
                OpCode::Close => return Err(WebSocketError::CloseFrameReceived)
            }
        }
    }

    pub fn register<F: FnMut() -> Result<(), WebSocketError> + 'static + Send + Sync>(&mut self, callback: F) {
        let cell = Box::new(callback);
        self.ping_callback = Some(cell);
    }
}

impl WebSocketReader<TcpStream> {
    pub fn set_read_timeout(&self, dur: Option<Duration>) -> io::Result<()> {
        self.reader.set_read_timeout(dur)
    }

    pub fn shutdown(&self) -> io::Result<()>{
        self.reader.shutdown(Shutdown::Read)
    }
}

impl WebSocketReader<TLSReader<TcpStream>> {
    pub fn set_read_timeout(&self, dur: Option<Duration>) -> io::Result<()> {
        self.reader.set_read_timeout(dur)
    }

    pub fn shutdown(&self) -> io::Result<()> {
        self.reader.shutdown()
    }
}