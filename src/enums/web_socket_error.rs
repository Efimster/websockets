use std::io;
use std::num::{ParseIntError};
use std::io::ErrorKind;
use http::enums::http_error::HttpError;
use tls::enums::tls_error::TLSError;
use std::sync::{PoisonError};

#[derive(Debug, Clone)]
pub enum WebSocketError {
    UnexpectedFrameOpcode,
    Origin,
    EmptyFrame,
    MalformedFrame,
    MalformedFrameLength,
    MissingHandshakeParameter,
    HandshakeRejected,
    IO(io::ErrorKind),
    Http(HttpError),
    TLS(TLSError),
    CloseFrameReceived,
    ThreadPoisoned,
}

impl From<io::Error> for WebSocketError {
    fn from(error: io::Error) -> Self {
        WebSocketError::IO(error.kind())
    }
}

impl From<ParseIntError> for WebSocketError {
    fn from(_: ParseIntError) -> Self { WebSocketError::IO(ErrorKind::InvalidInput) }
}

impl From<WebSocketError> for io::Error {
    fn from(error: WebSocketError) -> Self {
        match error {
            WebSocketError::IO(kind) => kind.into(),
            _ => ErrorKind::InvalidInput.into(),
        }
    }
}

impl From<HttpError> for WebSocketError {
    fn from(error: HttpError) -> Self {
        WebSocketError::Http(error)
    }
}

impl From<TLSError> for WebSocketError {
    fn from(error: TLSError) -> Self {
        WebSocketError::TLS(error)
    }
}


impl<Guard> From<PoisonError<Guard>> for WebSocketError {
    fn from(_: PoisonError<Guard>) -> Self {
        WebSocketError::ThreadPoisoned
    }
}