use crate::enums::web_socket_error::WebSocketError;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum OpCode {
    Continuation,
    Text,
    Binary,
    Close,
    Ping,
    Pong
}

impl OpCode {
    pub fn to_u8(&self) -> u8 {
        match self {
            OpCode::Continuation => 0,
            OpCode::Text => 1,
            OpCode::Binary => 2,
            OpCode::Close => 8,
            OpCode::Ping => 9,
            OpCode::Pong => 10
        }
    }

    pub fn from_u8(byte: u8) -> Result<OpCode, WebSocketError> {
        match byte {
            0 => Ok(OpCode::Continuation),
            1 => Ok(OpCode::Text),
            2 => Ok(OpCode::Binary),
            8 => Ok(OpCode::Close),
            9 => Ok(OpCode::Ping),
            10 => Ok(OpCode::Pong),
            _ => Err(WebSocketError::UnexpectedFrameOpcode)
        }
    }

    pub fn is_heartbeat(&self) -> bool {
        match self {
            OpCode::Ping | OpCode::Pong => true,
            _ => false,
        }
    }
}