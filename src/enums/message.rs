#[derive(Debug)]
pub enum Message<'a> {
    Ping(&'a [u8]),
    Pong(&'a [u8]),
    Close(&'a [u8]),
    Text(&'a [u8]),
    Binary(&'a [u8]),
}